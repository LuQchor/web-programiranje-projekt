<?php
session_start();


if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}

$action = $_POST['action'] ?? $_GET['action'] ?? ''; // Get action from POST or GET

switch ($action) {
    case 'add':
        $productId = $_POST['productId'];
        $quantity = $_POST['quantity'];
        $productName = $_POST['productName'];
        $price = $_POST['price'];
        $category = $_POST['category'];
        $imageSrc = $_POST['imageSrc'];

        if (isset($_SESSION['cart'][$productId])) {
            $_SESSION['cart'][$productId]['quantity'] += $quantity;
        } else {
            $_SESSION['cart'][$productId] = array(
                'name' => $productName,
                'quantity' => $quantity,
                'price' => $price,
                'category' => $category,
                'imageSrc' => $imageSrc
            );
        }

        echo json_encode(array('status' => 'success', 'message' => 'Product added to cart'));
        break;

    case 'get':
        include "spoj.php";
        $conn = new DatabaseConnection;
        $conn->connect();
        
        foreach ($_SESSION['cart'] as $productId => $item) {
            $sql = "SELECT kolicina FROM proizvodi WHERE id = $productId";
            $result = $conn->query($sql);
            $row = $conn->getArray($result);
            $_SESSION['cart'][$productId]['maxQuantity'] = $row['kolicina'];
        }

        echo json_encode($_SESSION['cart']);
        break;

    case 'remove':
        $productId = $_POST['productId'];
        unset($_SESSION['cart'][$productId]);
        echo json_encode(array('status' => 'success', 'message' => 'Product removed from cart'));
        break;

    default:
        echo json_encode(array('status' => 'error', 'message' => 'Invalid action'));
        break;
}
?>