<?php
include "spoj.php";
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Webshop</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <div class="container-fluid">
        <?php
        if (!isset($_SESSION['ime'])) {
            echo "<br><h5>Morate biti prijavljeni kako biste mogli naručiti proizvod.</h5>";
        }
        ?>

    </div>

    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>Ime proizvoda</th>
                <th>Opis</th>
                <th>Dostupna količina</th>
                <th>Cijena</th>
                <th>Kategorija</th>
                <th>Slika</th>
                <th>Naruči</th>
            </tr>

            <?php
            $conn = new DatabaseConnection;
            $conn->connect();

            $sql = "SELECT * FROM proizvodi";
            $resultAll = $conn->query($sql);

            if ($conn->getCount($resultAll) > 0) {
                while ($row = $conn->getArray($resultAll)) {
                    echo "<tr>";
                    echo "<td>" . $row['proizvod'] . "</td>";
                    echo "<td>" . $row['opis'] . "</td>";
                    echo "<td>" . $row['kolicina'] . "</td>";
                    echo "<td>" . $row['cijena'] . "</td>";
                    echo "<td>" . $row['kategorija'] . "</td>";
                    echo "<td><img src='" . $row['slika'] . "' alt='Slika proizvoda' class='image-product'></td>";
                    echo "<td><input type='number' min='1' max='" . $row['kolicina'] . "' value='1' id='quantity_" . $row['id'] . "' /> 
                    <button class='add-to-cart-btn' onclick='addToCart(" . $row['id'] . ",document.getElementById(\"quantity_" . $row['id'] . "\").value,
                    " . $row['kolicina'] . ", " . $row['cijena'] . ", \"" . $row['kategorija'] . "\", \"" . str_replace('\\', '\\\\', $row['slika']) . "\")'>Add to Cart</button> </td>";
                    echo "</tr>";
                }
            }
            ?>
        </table>
    </div>

    <div id="myModal" class="modal">
        <span class="close" id="modalCloseBtn">&times;</span>
        <img class="modal-content" id="modalImage">
    </div>

</body>

</html>