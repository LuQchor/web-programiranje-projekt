function displayCartForCheckout() {
    var checkoutTableBody = document.getElementById("checkoutTableBody");
    checkoutTableBody.innerHTML = "";
    var totalPriceSum = 0;

    for (var productId in cart) {
        var productName = cart[productId].name;
        var productQuantity = cart[productId].quantity;
        var productPrice = parseFloat(cart[productId].price);
        var productImage = cart[productId].imageSrc;
        var productCategory = cart[productId].category;
        var totalPrice = productQuantity * productPrice;
        totalPriceSum += totalPrice;
        

        var row = "<tr>";
        row += "<td>" + productName + "</td>";
        row += "<td>" + productQuantity + "</td>";
        row += "<td>" + productPrice.toFixed(2) + "€</td>";
        row += "<td>" + totalPrice.toFixed(2) + "€</td>";
        row += "<td><img src='" + productImage + "' alt='Slika proizvoda' class='image-product'></td>";
        row += "<td>" + productCategory + "</td>";
        row += "</tr>";

        checkoutTableBody.innerHTML += row;
        document.getElementById("cijena").value = totalPriceSum.toFixed(2);
    }

    var totalRow = "<tr>";
    totalRow += "<td colspan='5'>Vaša ukupna cijena je:</td>";
    totalRow += "<td>" + totalPriceSum.toFixed(2) + "€</td>";
    totalRow += "</tr>";

    checkoutTableBody.innerHTML += totalRow;
}

// Once the page is fully loaded, fetch and display cart items
window.onload = function () {
    fetchUpdatedCart(displayCartForCheckout);
};

document.addEventListener('DOMContentLoaded', function() {
    var confirmButton = document.getElementById('button_order');
    
    if (confirmButton) {
        confirmButton.addEventListener('click', function(event) {
            if (!confirm("Potvrdi narudžbu")) {
                event.preventDefault();
            }
        });
    }
});