<?php
include_once "spoj.php";

session_start();

?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <script src="queryFunctions.js"></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Odgovori na upite</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <div class="container-fluid">
        <div class="elem-group">
            <form method="post">
                <label for="message"></label>
                <textarea id="visitor_message_answer" name="visitor_message_answer"
                    placeholder="Odgovori na upit" required></textarea><br><br>
                <input type="hidden" name="checkbox_id_h" value="prazan" id="checkbox_id_h" />
                <button id="button_message_answer" type="submit" onclick="confirm2()">Odgovori na upit</button>
                <br>
            </form>
        </div>
    </div>

    <?php

    $conn = new DatabaseConnection;
    $conn->connect();

    $sql = "SELECT * FROM upiti WHERE odgovor = '' ";
    $resultAll = $conn->query($sql);

    if (!$resultAll) {
        die($conn->error());
    }

    if ($conn->getCount($resultAll) > 0) {

        echo '<div class="table-responsive">';
        echo "<table class=\"table\">";
        echo "<tr>";
        echo "<th>" . 'Ime' . "</th>";
        echo "<th>" . 'Prezime' . "</th>";
        echo "<th>" . 'Kontakt broj' . "</th>";
        echo "<th>" . 'Upit' . "</th>";
        echo "<th>" . 'ID' . "</th>";
        echo "</tr>";
        while ($row = $conn->getArray($resultAll)) {
            echo "<tr>";
            echo "<td>" . $row['ime'] . "</td>";
            echo "<td>" . $row['prezime'] . "</td>";
            echo "<td>" . $row['kontakt_broj'] . "</td>";
            echo "<td>" . $row['upit'] . "</td>";
            echo "<td><input type='checkbox' onclick='onlyOne(this)' name='checkbox_answer' value='" . $row['id'] . "'>" . $row['id'] . " </td>";
            echo "</tr>";
        }

        echo "</table>";
        echo "</div>";

    }

    ?>

    <?php
    if ($isTouch = isset($_POST['visitor_message_answer']) != 0 and $isTouch = isset($_SESSION['k_ime']) == 'admin' and $_POST["checkbox_id_h"] != 'prazan') {
        $id_h = $_POST["checkbox_id_h"];
        $conn = new DatabaseConnection;
        $conn->connect();

        $sql = "UPDATE upiti SET odgovor = '{$_POST['visitor_message_answer']}' WHERE id = '$id_h'";
        if ($conn->query($sql)) {

        } else {
            echo "Error: " . $sql . ": -" . $conn->error();
        }
        $conn->close();
    }
    ?>



    </html>