<?php
include_once 'spoj.php';
session_start();
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <script src="cartCheckout.js" defer></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Naruci</title>
</head>

<body>
    <?php include('izbornik.html'); ?>

    <div class="container-fluid">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>Ime proizvoda</th>
                    <th>Količina</th>
                    <th>Cijena</th>
                    <th>Ukupno</th>
                    <th>Slika</th>
                    <th>Kategorija</th>
                </tr>
                <tbody id="checkoutTableBody">
                   <!-- Cart items will be inserted here -->
                </tbody>
            </table>
        </div>
    </div>


    <form action="orderProcess.php" method="post" class="checkout-form">
        <div class="center-container">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-6">
                        <div class="text-center">
                            <label for="ulica" class="form-label">Ulica i kućni broj</label>
                            <input type="text" class="form-control" id="ulica" name="ulica" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label for="mjesto" class="form-label">Mjesto</label>
                            <input type="text" class="form-control" id="mjesto" name="mjesto" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label for="postal" class="form-label">Poštanski broj</label>
                            <input type="text" class="form-control" id="postal" name="postal" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label for="dodatne_informacije" class="form-label">Dodatne informacije</label>
                            <textarea id="dodatne_informacije" name="dodatne_informacije" rows="7" cols="50"></textarea>
                        </div>
                        <br>
                        <div class="text-center">
                            <input id="button_order" type="submit" value="Potvrdi narudžbu">
                        </div>
                        <br>
                        <input type="hidden" name="cijena" id="cijena">
                    </div>
                </div>
            </div>
        </div>
    </form>

</body>

</html>