<?php
include_once "spoj.php";

session_start();
if ($isTouch = isset($_POST['visitor_message']) != 0 and $isTouch = isset($_SESSION['k_ime']) != 0) {

    $conn = new DatabaseConnection;
    $conn->connect();

    $sql = "INSERT INTO upiti (ime,prezime,e_mail,k_ime,kontakt_broj,upit) VALUES('{$_SESSION['ime']}','{$_SESSION['prezime']}','{$_SESSION['e_mail']}','{$_SESSION['k_ime']}','{$_SESSION['kontakt_broj']}','{$_POST['visitor_message']}')";
    if ($conn->query($sql)) {

    } else {

        echo "Error: " . $sql . ": -" . $conn->error();
    }
    $conn->close();

}
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <script src="queryFunctions.js"></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Upit</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <?php
    if ($isTouch = isset($_SESSION['ime']) == 0) {
        echo "<br><h5>Morate biti prijavljeni kako biste poslali upit.</h5>";
    }
    ?>
    <div class="container-fluid">
        <div class="elem-group">
            <form method="post">
                <label for="message"></label>
                <textarea id="visitor_message" name="visitor_message"
                    placeholder="Što vas interesira?" required></textarea><br><br>
                <button id="button_message" type="submit" onclick="confirmQuery()">Pošalji upit</button>
                <br>
            </form>
        </div>
    </div>

    <div>

        <?php
        if ($isTouch = isset($_SESSION['k_ime']) != 0) {
            $conn = new DatabaseConnection;
            $conn->connect();

            $sql3 = "SELECT * FROM upiti WHERE k_ime = '" . $_SESSION['k_ime'] . "'";
            $resultAll3 = $conn->query($sql3);

            if (!$resultAll3) {
                die($conn->error());
            }

            if ($conn->getCount($resultAll3) > 0) {
                echo "</br>";
                echo '<div class="table-responsive">';
                echo "<table class=\"table\">";
                echo "<tr>";
                echo "<th>" . 'Upit' . "</th>";
                echo "<th>" . 'Odgovor' . "</th>";
                echo "</tr>";
                while ($row = $conn->getArray($resultAll3)) {
                    echo "<tr>";
                    echo "<td>" . $row['upit'] . "</td>";
                    echo "<td>" . $row['odgovor'] . "</td>";
                    echo "</tr>";
                }

                echo "</table>";
                echo "</div>";

            }
        }
        ?>
    </div>
</body>

</html>