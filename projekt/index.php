<?php
include_once 'spoj.php';
session_start();
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <link rel="stylesheet" href="mojcss.css">

    <title>Pocetna</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <?php
                if ($isTouch = isset($_SESSION['ime']) == 0) {
                    echo "<h5>Dobro došli</h5>";
                } else {
                    $ime = $_SESSION['ime'];
                    $prezime = $_SESSION['prezime'];
                    echo "<h5>Dobro došli $ime $prezime </h5>";
                }
                ?>
            </div>
        </div>
    </div>

    <div class="index-container">
        <div class="left-container">
            <p class="text-element">Dobrodošli u naš svijet posvećen okusima i tradiciji – mjesto gdje se strast prema
                prirodno uzgojenim namirnicama iz vlastitog vrta spaja s ljubavlju prema domaćim slasticama. Naša
                obiteljska
                proizvodnja odražava ljubav prema hrani i nasljeđe koje nosimo sa sobom.

                Naša mala farma pruža nam raznovrsno povrće, voće i začine, svi pažljivo uzgojeni kako bi se očuvala
                prirodna ravnoteža. Svaka namirnica koja napušta naše posjede donosi s njom okus svježine i brižno rukom
                uzgojene kvalitete.

                No naša strast se ne zaustavlja kod povrća. Naša kuhinja je ispunjena mirisima svježe pečenih i mirisnih
                kolača te jedinstvenih slastica. Naši recepti nasljeđuju se generacijama i pružaju vam okus topline doma
                i
                tradicije u svakom zalogaju.

                Naš cilj je stvoriti posebne trenutke za vas. Naše torte i slastice su više od hrane – one su umjetnost
                koja
                spaja okuse i vizualnu ljepotu. Bez obzira na to je li poseban dan u pitanju ili samo trenutak žudnje za
                nečim slatkim, naše kreacije su tu da obogate svaki trenutak.

                Naše poslovanje utemeljeno je na predanosti. Predani smo vama, našim cijenjenim kupcima, kako bismo vam
                pružili najbolje gastronomske užitke. Također smo predani prirodi i njenom očuvanju te podržavamo
                održivi
                način života. Kroz svaku porciju i komadić hrane želimo prenijeti iskru tradicije i ljubavi kojom je
                stvorena.

                Hvala što ste dio naše priče. Pozivamo vas da istražite svijet okusa koji spaja prirodu i slatke
                trenutke
                života.</p>
            <div id="image-3"><img src="Slike\35.jpg" alt="Image 3"></div>
        </div>
        <div class="right-container-1">
            <div id="image-2"><img src="Slike\4.jpg" alt="Image 2"></div>
        </div>
        <div class="right-container-2">
            <div id="image-1"><img src="Slike\3.jpg" alt="Image 1"></div>
        </div>
    </div>

</body>

</html>