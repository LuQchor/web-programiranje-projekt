<?php
include_once "spoj.php";

session_start();
if ($_SESSION['ime'] == NULL) {
    echo 'neispravna sesija';
    header("location: index.php");
}
if ($_SESSION['uloga'] == 'admin') {

    $conn = new DatabaseConnection;
    $conn->connect();

    if (isset($_POST['proizvod'])) {
        $id = $_POST['id'];
        $proizvod = $_POST['proizvod'];
        $opis = $_POST['opis'];
        $kolicina = $_POST['kolicina'];
        $cijena = $_POST['cijena'];
        $kategorija = $_POST['kategorija'];
        $slika = $_POST['slika'];

        $stmt = $conn->getConnection()->prepare("INSERT INTO proizvodi(id,proizvod,opis,kolicina,cijena,kategorija,slika) VALUES (?, ?, ?, ?, ?, ?, ?)");

        $stmt->bind_param("sssisss", $id, $proizvod, $opis, $kolicina, $cijena, $kategorija, $slika);

        if ($stmt->execute()) {
            echo "Unos uspiješan";
            exit;
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
        $conn->close();
    }
} else
    header("location:index.php");
?>