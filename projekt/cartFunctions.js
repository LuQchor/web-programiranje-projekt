var cart = {};
        var modal = document.getElementById("myModal");
        var images = document.querySelectorAll(".image-product");
        var modalImg = document.getElementById("modalImage");
        var modalCloseBtn = document.getElementById("modalCloseBtn");

        
        

        function addToCart(productId, quantity, maxQuantity, price, category, imageSrc) {
            // If the product exists in the cart, get its name, otherwise, fetch it from the DOM.
            var productName = cart[productId] && cart[productId].name 
                ? cart[productId].name 
                : document.querySelector("#quantity_" + productId)
                    ? document.querySelector("#quantity_" + productId).closest("tr").children[0].textContent
                    : "";
        
            var currentQuantity = cart[productId] && cart[productId].quantity ? parseInt(cart[productId].quantity) : 0;
            var newQuantity = currentQuantity + parseInt(quantity);
        
            if (newQuantity > maxQuantity) {
                alert("Nemožete dodati više od  " + maxQuantity + " od ovog proizvoda u košaricu.");
                return;
            }
        
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'cartScripts.php', true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    fetchUpdatedCart();
                }
            };
            xhr.send("action=add&productId=" + productId + "&quantity=" + quantity + "&productName=" + encodeURIComponent(productName) + "&price=" + price + "&category=" + encodeURIComponent(category) + "&imageSrc=" + encodeURIComponent(imageSrc));

        }
        
        
        
        
        function fetchUpdatedCart(callback) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'cartScripts.php?action=get', true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    cart = JSON.parse(xhr.responseText);
                    updateCartDropdown(cart);
                    if (callback) {
                        callback();  // Execute the callback if provided
                    }
                }
            };
            xhr.send();
        }
        fetchUpdatedCart();
        
        var cartButton = document.getElementById("cartButton");
        var cartDropdown = document.getElementById("cartDropdown");
        
        cartButton.addEventListener("click", function () {
            if (cartDropdown.style.display === "none" || cartDropdown.style.display === "") {
                fetchUpdatedCart();
                cartDropdown.style.display = "block";
            } else {
                cartDropdown.style.display = "none";
            }
        });
        
        function updateCartDropdown(cart) {
            var cartTableBody = document.getElementById("cartTableBody");
            cartTableBody.innerHTML = "";
            var totalPriceSum = 0;

            for (var productId in cart) {
                var productName = cart[productId].name;
                var productQuantity = cart[productId].quantity;
                var maxQuantityElem = document.querySelector("#quantity_" + productId);
                var maxQuantity = maxQuantityElem ? parseInt(maxQuantityElem.getAttribute("max")) : 0;
                var productPrice = cart[productId].price;
                var totalPrice = productQuantity * productPrice;
                totalPriceSum += totalPrice;
                
                var row = "<tr>";
                row += "<td>" + productName + "</td>";
                row += "<td>" + productQuantity + "</td>";
                row += "<td>";
                row += "<button onclick='modifyCartQuantity(" + productId + ", 1)'>+</button>";
                row += "<button onclick='modifyCartQuantity(" + productId + ", -1)'>-</button>";
                row += "<button onclick='removeFromCart(" + productId + ")'>x</button>";
                row += "</td>";
                row += "<td>" + totalPrice.toFixed(2) + "€</td>"; 
                row += "</tr>";
                
                cartTableBody.innerHTML += row;
            }
            
            var totalRow = "<tr>";
            totalRow += "<td colspan='3'>Vaša ukupna cijena je:</td>";
            totalRow += "<td>" + totalPriceSum.toFixed(2) + "€</td>";
            totalRow += "</tr>";
            
            cartTableBody.innerHTML += totalRow;
            
            
            if (cartTableBody.innerHTML === "") {
                cartTableBody.innerHTML = "<tr><td colspan='4'>Your cart is empty!</td></tr>";
            }

            let totalItemCount = getItemCountInCart();
            let cartItemCountElement = document.getElementById("cartItemCount");
            cartItemCountElement.textContent = totalItemCount;
            
            if (totalItemCount === 0) {
                cartItemCountElement.style.display = 'none';
            } else {
                cartItemCountElement.style.display = 'inline';
            }
            
        
        }


        function modifyCartQuantity(productId, change) {
            var currentQuantity = cart[productId] && cart[productId].quantity ? parseInt(cart[productId].quantity) : 0;
            var newQuantity = currentQuantity + change;
            
            var maxQuantity = cart[productId] && cart[productId].maxQuantity ? parseInt(cart[productId].maxQuantity) : 0;
        
            console.log("Current Quantity:", currentQuantity, "New Quantity:", newQuantity);
            
            if (newQuantity > maxQuantity) {
                alert("Nemožete dodati više od  " + maxQuantity + " od ovog proizvoda u košaricu.");
                return;
            } else if (newQuantity <= 0) {
                removeFromCart(productId);
                return;
            } else {
                addToCart(productId, change, maxQuantity);
            }
        }
        
        
        
        
        function removeFromCart(productId) {
            if (confirm("Jeste li sigurni da želite ukloniti ovaj proizvod iz košarice?")) {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'cartScripts.php', true);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        fetchUpdatedCart();
                    }
                };
                xhr.send("action=remove&productId=" + productId);
            }
        }
        
        function getItemCountInCart() {
            let itemCount = 0;
            for (let productId in cart) {
                itemCount += parseInt(cart[productId].quantity);
            }
            return itemCount;
        }
        


        //modal functions
        images.forEach(function (image) {
            image.addEventListener("click", function () {
                modal.style.display = "block";
                modalImg.src = this.src;
            });
        });

        window.onclick = function (event) {
            if (event.target === modal || event.target === modalCloseBtn) {
                modal.style.display = "none";
                modalImg.style.width = "auto";
                modalImg.style.height = "auto";
            }
        };

        
