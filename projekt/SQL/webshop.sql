-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2023 at 02:21 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(11) NOT NULL,
  `ime` varchar(63) NOT NULL,
  `prezime` varchar(63) NOT NULL,
  `e_mail` varchar(63) NOT NULL,
  `k_ime` varchar(63) NOT NULL,
  `lozinka` varchar(63) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `kontakt_broj` varchar(63) NOT NULL,
  `uloga` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `ime`, `prezime`, `e_mail`, `k_ime`, `lozinka`, `kontakt_broj`, `uloga`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', 'admin', 'admin', '9999999', 'admin'),
(2, 'Luka', 'Malnar', 'lmalnar@etfos.hr', 'lmalnar', 'Luka123!', '0915816256', 'korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `narudzbe`
--

CREATE TABLE `narudzbe` (
  `id` int(11) NOT NULL,
  `ime` varchar(63) NOT NULL,
  `prezime` varchar(63) NOT NULL,
  `e_mail` varchar(63) NOT NULL,
  `k_ime` varchar(63) NOT NULL,
  `kontakt_broj` varchar(63) NOT NULL,
  `narudzba` varchar(8191) NOT NULL,
  `ulica i kucni broj` varchar(127) NOT NULL,
  `mjesto` varchar(127) NOT NULL,
  `postanski broj` varchar(63) NOT NULL,
  `dodatne informacije` varchar(8191) NOT NULL,
  `cijena` varchar(63) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `narudzbe`
--

INSERT INTO `narudzbe` (`id`, `ime`, `prezime`, `e_mail`, `k_ime`, `kontakt_broj`, `narudzba`, `ulica i kucni broj`, `mjesto`, `postanski broj`, `dodatne informacije`, `cijena`) VALUES
(9, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', '{\"1\":{\"name\":\"Sok od kru\\u0161ke 1.5L\",\"quantity\":\"1\",\"price\":\"2\",\"category\":\"Pi\\u0107e\"},\"2\":{\"name\":\"Kupus kifle\",\"quantity\":\"1\",\"price\":\"0.77\",\"category\":\"Poslastice\"}}', '123', '123', '123', '', '2.77'),
(19, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', '{\"1\":{\"name\":\"Sok od kru\\u0161ke 1.5L\",\"quantity\":\"1\",\"price\":\"2\",\"category\":\"Pi\\u0107e\"},\"2\":{\"name\":\"Kupus kifle\",\"quantity\":\"1\",\"price\":\"0.77\",\"category\":\"Poslastice\"}}', '123', '123', '123', '', '2.77'),
(21, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', '{\"6\":{\"name\":\"Zvjezdice s pekmezom\",\"quantity\":2,\"price\":\"0.22\",\"category\":\"Poslastice\"}}', 'f', 'f', 'f', 'f', '0.44'),
(22, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', '{\"7\":{\"name\":\"\\u010cokoladni polumjesec keksi\\u0107i\",\"quantity\":\"1\",\"price\":\"0.12\",\"category\":\"Poslastice\"}}', 'ads', 'asd', 'asd', '', '0.12'),
(23, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', '{\"7\":{\"name\":\"\\u010cokoladni polumjesec keksi\\u0107i\",\"quantity\":1,\"price\":\"0.12\",\"category\":\"Poslastice\"}}', 'asd', 'asd', 'asd', '', '0.12');

-- --------------------------------------------------------

--
-- Table structure for table `proizvodi`
--

CREATE TABLE `proizvodi` (
  `id` int(5) NOT NULL,
  `proizvod` varchar(127) NOT NULL,
  `opis` varchar(1023) NOT NULL,
  `kolicina` int(5) NOT NULL,
  `cijena` varchar(63) NOT NULL,
  `kategorija` varchar(255) NOT NULL,
  `slika` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `proizvodi`
--

INSERT INTO `proizvodi` (`id`, `proizvod`, `opis`, `kolicina`, `cijena`, `kategorija`, `slika`) VALUES
(1, 'Sok od kruške 1.5L', 'Osvežavajući napitak koji sjedinjuje prirodnu slatkoću krušaka s osvežavajućim karakterom voćnog soka. Svaka kap ovog raskošnog napitka prenosi miris sveže ubranih krušaka i obećava pravu eksploziju voćnih ukusa.', 49, '2', 'Piće', 'Slike\\31.jpg'),
(2, 'Kupus kifle', 'Ukusna i aromatična peciva koja se ističu svojim bogatim ukusom i teksturama. Ova popularna poslastica spaja savršen balans između mekoće i hrskavosti, pružajući neodoljivu kombinaciju svakog zalogaja.', 19, '0.77', 'Poslastice', 'Slike\\11.jpg'),
(3, 'Cikla', 'Ova teglica sadrži pažljivo pripremljene komade sveže cikle koji su ukiseljeni u savršenoj kombinaciji začina i sirćeta, stvarajući izvanredan spoj slatko-kiselog ukusa koji oduševljava okusne pupoljke.', 80, '2', 'Povrće', 'Slike\\26.jpg'),
(4, 'Pekmez od marelice', 'Svaki zalogaj obiluje okusom sočnih i zrelih marelica koje su pažljivo pripremljene i pomiješane s nježnom slatkoćom. Ovaj pekmez ima prepoznatljivu svijetlu boju i glatku teksturu koja se lako razmazuje.', 45, '5', 'Pekmez', 'Slike\\32.jpg'),
(5, 'Pekemez od mješanog voća', 'Domaći pekmez od mješanog voća sastoji se od najsočnijih plodova kao što su jagode, maline, šljive i breskve. S obzirom na raznolikost voćnih sastojaka, pekmez se ističe bogatom svijetloružičastom bojom i finom zrnastom strukturom. Savršen je kao namaz na tostu ili palačinkama te kao dodatak jogurtu ili desertima.', 45, '5', 'Pekmez', 'Slike\\33.jpg'),
(6, 'Zvjezdice s pekmezom', 'Zvjezdice ispunjene slatkim pekmezom po želji. Svaka zvjezdica je delikatna ravnoteža hrskave vanjštine i sočne unutrašnjosti. ', 98, '0.22', 'Poslastice', 'Slike\\29.jpg'),
(7, 'Čokoladni polumjesec keksići', 'Oblik polumjeseca ne samo da dodaje estetski užitak, već pruža i punoću okusa. Idealni su za uživanje uz šalicu tople kave ili čaja', 198, '0.12', 'Poslastice', 'Slike\\9.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `upiti`
--

CREATE TABLE `upiti` (
  `id` int(11) NOT NULL,
  `ime` varchar(63) NOT NULL,
  `prezime` varchar(63) NOT NULL,
  `e_mail` varchar(63) NOT NULL,
  `k_ime` varchar(63) NOT NULL,
  `kontakt_broj` varchar(63) NOT NULL,
  `upit` varchar(3071) NOT NULL,
  `odgovor` varchar(3071) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `upiti`
--

INSERT INTO `upiti` (`id`, `ime`, `prezime`, `e_mail`, `k_ime`, `kontakt_broj`, `upit`, `odgovor`) VALUES
(112, 'Luka', 'Malnar', 'lmalnar@etfos.hr', 'lmalnar', '0915816256', 'Poštovani zanima me koliko košta teglica kajsijinog meda.\r\n', '5 € za količine do 10 teglica \r\nza 10 i više teglica 4.5 €'),
(113, 'admin', 'admin', 'admin@admin.com', 'admin', '9999999', 'Zanima me dali je moguće naručiti veće količine kako za firmu i dali su mogući popusti LP.', ''),
(114, 'Luka', 'Malnar', 'lmalnar@etfos.hr', 'lmalnar', '0915816256', 'Odakle ste', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `narudzbe`
--
ALTER TABLE `narudzbe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proizvodi`
--
ALTER TABLE `proizvodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upiti`
--
ALTER TABLE `upiti`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `narudzbe`
--
ALTER TABLE `narudzbe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `proizvodi`
--
ALTER TABLE `proizvodi`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `upiti`
--
ALTER TABLE `upiti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
