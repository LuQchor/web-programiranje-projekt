function confirmQuery() {
    if ("<?php echo $isTouch = isset($_SESSION['k_ime'])>" != 0) {
        alert("Morate biti prijavljeni kako biste poslali upit");
        return;
    }
    
    var PostojiPoruka = document.getElementById("visitor_message").value;
    
    if ((PostojiPoruka != 0) && ("<?php echo $_SESSION['k_ime'] ?>" != 0)) {
        alert("Uspješno ste predali upit");
    } else {
        alert("Napišite upit.");
    }
}

var checkbox_id = NULL;
function onlyOne(checkbox) {
    var checkboxes = document.getElementsByName('checkbox_answer')

    checkboxes.forEach((item) => {
        if (item !== checkbox) {
            item.checked = false
        }
        else {
            checkbox_id = checkbox.value;
            document.getElementById("checkbox_id_h").value = checkbox_id;
        }
    })
}
function confirm2() {
    var PostojiPoruka = document.getElementById("visitor_message_answer").value;
    if ((PostojiPoruka != 0) && ("<?php echo $_SESSION['k_ime'] ?>" != 0) && (checkbox_id != null)) {
        alert("Uspješno ste odgovorili na upit " + checkbox_id);
    } else if (PostojiPoruka == 0) {
        alert("Odgovorite na upit.");
    }
    else if ((checkbox_id == null)) {
        alert("Odaberite upit na koji želite odgovoriti.");
    }
}

