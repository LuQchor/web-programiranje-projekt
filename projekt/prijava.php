<?php
include "spoj.php";
session_start();
// U bazi ne smije biti utf8.... ci (ci-case insensitive)
if ($_POST) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $conn = new DatabaseConnection;
        $conn->connect();

        $myusername = mysqli_real_escape_string($conn->getConnection(), $_POST['username']);
        $mypassword = mysqli_real_escape_string($conn->getConnection(), $_POST['password']);

        $sql = "SELECT id from korisnici WHERE k_ime = '$myusername' and lozinka = '$mypassword'";

        $result = $conn->query($sql);
        $row = $conn->getArray($result);
        $count = $conn->getCount($result);

        if ($isTouch = isset($row['id']) != 0) {
            $id = $row['id'];

            $sql2 = "SELECT * FROM korisnici WHERE id = '$id'";
            $resultAll = $conn->query($sql2);

            if (!$resultAll) {
                die($conn->error());
            }

            if ($conn->getCount($resultAll) > 0) {
                $rowData = $conn->getArray($resultAll);
            }

            if ($count == 1) {
                $_SESSION['ime'] = $rowData['ime'];
                $_SESSION['prezime'] = $rowData['prezime'];
                $_SESSION['uloga'] = $rowData['uloga'];
                $_SESSION['id'] = $rowData['id'];
                $_SESSION['kontakt_broj'] = $rowData['kontakt_broj'];
                $_SESSION['k_ime'] = $rowData['k_ime'];
                $_SESSION['e_mail'] = $rowData['e_mail'];

                if ($_SESSION['uloga'] != NULL) {
                    header("location: index.php");
                    exit();
                }
            }
        } else {
            echo "<label class=\"text-danger\">Neispravni podatci pokušajte ponovo.</label>";
        }
    }
}
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Prijava</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <div class="center-container">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-6">
                    <form action="" method="post">
                        <div class="text-center">
                            <label class="form-label" for="username">Korisničko ime</label>
                            <input class="form-control" type="text" name="username" id="username">
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="password">Lozinka</label>
                            <input class="form-control" type="password" name="password" id="password">
                        </div>
                        <br>
                        <div class="text-center">
                            <input id="button_login" type="submit" value="Prijavi se">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>