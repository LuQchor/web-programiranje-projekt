<?php
include_once 'spoj.php';

session_start();
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="cartFunctions.js" defer></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Dodaj proizvod</title>
</head>

<body>
    <?php include('izbornik.html'); ?>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-6">
                <form action="unos.php" method="post">
                    <div class="text-center">
                        <br>
                        <label class="form-label" for="proizvod">Ime proizvoda</label>
                        <input class="form-control" type="text" name="proizvod" id="proizvod">
                    </div>
                    <br>
                    <div class="text-center">
                        <label class="form-label" for="opis">Opis proizvoda</label>
                        <textarea class="form-control" name="opis" id="opis"></textarea>
                    </div>
                    <br>
                    <div class="text-center">
                        <label class="form-label" for="kolicina">Količina proizvoda</label>
                        <input class="form-control" type="number" name="kolicina" id="kolicina">
                    </div>
                    <br>
                    <div class="text-center">
                        <label class="form-label" for="cijena">Cijena proizvoda</label>
                        <input class="form-control" type="text" name="cijena" id="cijena">
                    </div>
                    <br>
                    <div class="text-center">
                        <label class="form-label" for="kategorija">Kategorija</label>
                        <select class="form-select" id="kategorija" name="kategorija">
                            <option value="Poslastice">Poslastice</option>
                            <option value="Piće">Piće</option>
                            <option value="Pekez">Pekez</option>
                            <option value="Povrće">Povrće</option>
                        </select>
                    </div>
                    <br>
                    <div class="text-center">
                        <label class="form-label" for="slika">Put do slike</label>
                        <input class="form-control" type="text" name="slika" id="slika">
                    </div>
                    <br>
                    <div class="text-center">
                        <input id="button_login" type="button" value="Dodaj!" onclick="submitForm();">
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>

    <script>
        function submitForm() {
            const form = document.querySelector('form');
            const formData = new FormData(form);

            fetch('unos.php', {
                method: 'POST',
                body: formData
            })
                .then(response => response.text())
                .then(result => {
                    if (result.includes("Unos uspiješan")) {
                        alert('Uspješan unos');
                        form.reset(); // Reset the form
                    } else {
                        alert('There was an error!');
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    alert('An error occurred!');
                });
        }
    </script>
</body>

</html>