<?php
include_once 'spoj.php'; // Assuming spoj.php contains your database connection details
$conn = new DatabaseConnection();
$conn->connect();

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_SESSION['k_ime']) || $_SESSION['k_ime'] == 0) {
    echo '<script>
    alert("Morate biti prijavljeni kako biste pristupili ovoj stranici.");
    window.location.href = "prijava.php";
  </script>';
    exit;
}

if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
    echo '<script>
            alert("Dodajte proizvode u košaricu.");
            window.location.href = "webshop.php";
          </script>';
    exit;
}
// Check if form data and cart exist
if ($_POST && isset($_SESSION['cart'])) {

    // Identify the logged-in user
    $loggedInUser = $_SESSION['k_ime']; // Replace with the appropriate session variable if different
    
    // Retrieve user details from korisnici table
    $sql = "SELECT ime, prezime FROM korisnici WHERE k_ime = ?";
    $stmt = $conn->getConnection()->prepare($sql);
    $stmt->bind_param("s", $loggedInUser);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($row = $result->fetch_assoc()) {
        $ime = $row['ime'];
        $prezime = $row['prezime'];
        $e_mail = $_SESSION['e_mail'];
        $k_ime = $_SESSION['k_ime'];
        $kontakt_broj = $_SESSION['kontakt_broj'];
    } else {
        echo "Error retrieving user details.";
        exit;
    }

    // Retrieve form data
    $ulica = $_POST['ulica'];
    $mjesto = $_POST['mjesto'];
    $postal = $_POST['postal'];
    $dodatne_informacije = $_POST['dodatne_informacije'];
    $cijena = $_POST['cijena'];

    // ... add any other form fields if present

    // Retrieve cart details and convert to a suitable format for storage
    $cart_data = $_SESSION['cart']; # Store cart data before clearing it
    foreach ($cart_data as $key => $item) {
        unset($item['imageSrc']);
        unset($item['maxQuantity']);
        $cart_data[$key] = $item;
    }
    $narudzba = json_encode($cart_data); // Convert cart array to JSON string

    // Insert order into database
    $sql = "INSERT INTO narudzbe (ime, prezime, e_mail, k_ime, kontakt_broj, narudzba, `ulica i kucni broj`, mjesto, `postanski broj`, `dodatne informacije`, cijena) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->getConnection()->prepare($sql);
    $stmt->bind_param("sssssssssss", $ime, $prezime, $e_mail, $k_ime, $kontakt_broj, $narudzba, $ulica, $mjesto, $postal, $dodatne_informacije, $cijena);
    // ... make sure to retrieve and bind all the necessary variables

    if ($stmt->execute()) {

        // Process cart items before clearing
        foreach ($_SESSION['cart'] as $productId => $item) {
            if (isset($item['quantity'])) {
                $ordered_quantity = intval($item['quantity']);

                error_log(print_r($_SESSION['cart'], true));
                error_log("Processing product ID: $productId");

                $sql = "SELECT kolicina FROM proizvodi WHERE id = ?";
                $stmt = $conn->getConnection()->prepare($sql);
                $stmt->bind_param("i", $productId);
                $stmt->execute();
                $result = $stmt->get_result();

                if ($row = $result->fetch_assoc()) {
                    $current_quantity = intval($row['kolicina']);
                    $new_quantity = max($current_quantity - $ordered_quantity, 0);

                    error_log("Current quantity for product $productId: $current_quantity");
                    error_log("Ordered quantity: $ordered_quantity");
                    error_log("New quantity: $new_quantity");

                    $sql = "UPDATE proizvodi SET kolicina = ? WHERE id = ?";
                    $stmt = $conn->getConnection()->prepare($sql);
                    $stmt->bind_param("ii", $new_quantity, $productId);
                    if ($stmt->execute()) {
                        error_log("Successfully updated quantity for product $productId");
                    } else {
                        error_log("Failed to update quantity for product $productId. Error: " . $stmt->error);
                    }
                }
            }
        }
        // Now, it's safe to clear the cart
        unset($_SESSION['cart']);
        echo '<script>
        alert("Vaša narudžba je uspješno poslana");
        window.location.href = "index.php";
      </script>';
    } else {
        echo "Error: " . $stmt->error;
    }

    $stmt->close();
} else {
    echo "Form data or cart not found.";
}

$conn->close();

error_log("Full session data: " . print_r($_SESSION, true));
?>