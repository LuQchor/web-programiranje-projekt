<?php

require_once "spoj.php";

if (isset($_POST['username'])) {

    $conn = new DatabaseConnection;
    $conn->connect();

    $sql = 'SELECT * FROM korisnici WHERE k_ime = "' . $_POST['username'] . '"';

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        echo "<label class=\"text-danger\">Korisničko ime zauzeto.</label>";

    } else {

        if (isset($_POST['username'])) {

            $ime = $_POST['ime'];
            $ime = ucfirst($ime);
            $prezime = $_POST['prezime'];
            $prezime = ucfirst($prezime);
            $email = $_POST['email'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $phone_number = $_POST['phone_number'];
            $sql = "INSERT INTO korisnici (ime,prezime,e_mail,k_ime,lozinka,kontakt_broj,uloga) VALUES('$ime','$prezime','$email','$username','$password','$phone_number','korisnik')";

            if ($conn->query($sql)) {
                header("location: index.php");
            } else {
                echo "Error: " . $sql . ": -" . $conn->error();
            }
            $conn->close();
        }
    }
}
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="mojcss.css">
    <title>Registracija</title>
</head>

<body>

    <?php include('izbornik.html'); ?>
    <div class="center-container">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-6">
                    <form onsubmit="return matchPassword()" method="post">
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="ime">Ime</label>
                            <input class="form-control" type="text" name="ime" id="ime" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="prezime">Prezime</label>
                            <input class="form-control" type="text" name="prezime" id="prezime" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="email">E-mail</label>
                            <input class="form-control" type="email" name="email" id="email" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="username">Korisničko ime</label>
                            <input class="form-control" type="text" name="username" id="username" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="password">Lozinka</label>
                            <input class="form-control" type="password" name="password" id="password"
                                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="password2">Potvrdi Lozinku</label>
                            <input class="form-control" type="password" name="password2" id="password2"
                                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                        </div>
                        <br>
                        <div class="text-center">
                            <label class="form-label" for="phone_number">Kontakt broj</label>
                            <input class="form-control" type="tel" name="phone_number" id="phone_number" pattern="\d+"
                                title="Please enter only numbers.">
                        </div>
                        <br>
                        <div class="text-center">
                            <input id="button_register" type="submit" value="Registriraj se">
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div id="password_message">
        <h3>Lozinka mora imati:</h3>
        <p id="letter" class="invalid"> <b>Malo</b> slovo</p>
        <p id="capital" class="invalid"><b>Veliko</b> slovo</p>
        <p id="number" class="invalid"><b>Broj</b></p>
        <p id="length" class="invalid">Najmanje <b>8 znakova</b></p>
    </div>

    <script>
        var myInput = document.getElementById("password");
        var myInput2 = document.getElementById("password2");

        // When the user clicks on the password field, show the message box
        myInput.onfocus = function () {
            document.getElementById("password_message").style.display = "block";
        }

        // When the user clicks outside of the password field, hide the message box
        myInput.onblur = function () {
            document.getElementById("password_message").style.display = "none";
        }


        myInput2.onfocus = function () {
            document.getElementById("password_message").style.display = "block";
        }

        myInput2.onblur = function () {
            document.getElementById("password_message").style.display = "none";
        }

        function matchPassword() {
            var pw1 = document.getElementById("password");
            var pw2 = document.getElementById("password2");
            if (pw1.value != pw2.value) {
                alert("Lozinke se ne podudaraju");
                return false;
            } else {
                return true;
            }
        } 
    </script>

    </div>

</body>

</html>